#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on %(date)s

@author: %(username)s
"""

from plyfile import PlyData, PlyElement
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d
from skimage import io
import time
from bisect import bisect
"""Gets and prints the spreadsheet's header columns

:param file_loc: The file location of the spreadsheet
:type file_loc: str
:param print_cols: A flag used to print the columns to the console
    (default is False) 
:type print_cols: bool
:returns: a list of strings representing the header columns
:rtype: list
"""


def accumulation (x_base,y_base,pas):
    x=(x_base-np.min(x_base))/pas
    y=(y_base-np.min(y_base))/pas
    etendue_x = int(np.ceil(np.max(x)))+1
    etendue_y = int(np.ceil(np.max(y)))+1
    mat=np.zeros((etendue_x,etendue_y))
    l=len(x)
    intx=x.astype(int)
    inty=y.astype(int)
    print(x,y)
    print(intx,inty)
    for i in range(l):
        mat[intx[i]][inty[i]]+=1
    return intx,inty,etendue_x,etendue_y,mat


def normale(P1,P2):
    #Calcule la normale à un vecteur de deux points
    vec=P2-P1
    norm=np.array((-vec[1],vec[0]))/np.linalg.norm(np.array((-vec[1],vec[0])))
    return norm

def distance(P1,P2):
    #Calcule la distance entre deux points
    vec=P2-P1
    dist=np.sum(vec**2)**0.5
    return dist

class Ouverture_ply():  
    def __init__(self, pathnuage, dishoraire): # The constructor
        self.pathnuage = pathnuage
        self.dis_horaire=dishoraire
        self.x = []
        self.y = []
        self.z = []
        self.GPS_time = []
        self.x_sensor = []
        self.y_sensor = []
        self.z_sensor = []
        self.x_origin_sensor = []
        self.y_origin_sensor = []
        self.z_origin_sensor = []
        self.x_origin = []
        self.y_origin = []
        self.z_origin = []
        self.range = []
        self.theta = []
        self.phi = []
        self.f_g=[]
        self.f_d=[]
        self.z_sensor_=2.25
        self.tolerance=0.25
        self.nbaccu=1000
        self.dx=np.array(self.x)-np.array(self.x_origin)
        self.dy=np.array(self.y)-np.array(self.y_origin)
        self.dz=np.array(self.z)-np.array(self.z_origin)
        ply_file=PlyData.read(self.pathnuage)
        self.plydata = ply_file.elements[0]
        self.data_=self.plydata.data
        self.dtype_=self.plydata.data.dtype
        self.name_=self.plydata.name
        self.coms= ply_file.comments
        self.outfile="test22.ply"
        
    def ouvrir_nuage(self):
        """Fonction primaire d'ouverture du nuage de points et de récupération de ses attributs 
        """

        plydata = PlyData.read(self.pathnuage)  
        self.x, self.y, self.z =  plydata.elements[0].data['x'], plydata.elements[0].data['y'], plydata.elements[0].data['z']
        self.GPS_time, self.x_sensor, self.y_sensor, self.z_sensor =  plydata.elements[0].data['GPS_time'], plydata.elements[0].data['x_sensor'], plydata.elements[0].data['y_sensor'], plydata.elements[0].data['z_sensor']
        self.x_origin_sensor, self.y_origin_sensor, self.z_origin_sensor, self.x_origin, self.y_origin, self.z_origin =  plydata.elements[0].data['x_origin_sensor'], plydata.elements[0].data['y_origin_sensor'], plydata.elements[0].data['z_origin_sensor'], plydata.elements[0].data['x_origin'], plydata.elements[0].data['y_origin'], plydata.elements[0].data['z_origin']
        self.range, self.theta, self.phi =  plydata.elements[0].data['range'], plydata.elements[0].data['theta'], plydata.elements[0].data['phi']
      
    
    def distance(self,p):
        """Retourne la distance du p_ième point à au point d'émission du laser
    
        :param p: l'identifant du point
        :type p: int
        :returns: la distance entre le point p et le p_ième point d'émission du laser
        :rtype: float
        """
        return np.sqrt(self.dx[p]**2+self.dy[p]**2+self.dz[p]**2)
    

    def delta_z(self,p1,p2):
        return self.z[p1]-self.z[p2]
    
    def rang_grossier(self):
        mod_r=(self.range-np.mean(self.range))/np.std(self.range)
        self.range_filter=self.range[np.where(mod_r<4)]

    
    def d_g_grossier(self):
        """Fait une segmentation grossière entre bords gauche et droite du nuage, selon l'angle de visée theta
        """
        self.d,self.g=[],[]
        for i in range(len(self.theta)):
            if self.theta[i]>0:
                self.d.append(i)
            else:
                self.g.append(i)
                
    def sursol_grossier(self):
        """Fait une segmentation grossière entre sol et sursol du nuage, selon leur altitude normalisée
        """
        self.znorm=np.array(np.array(self.z[:])-np.array(self.z_origin[:])+2.25)
        self.sursol_g=np.array(self.g)[np.where(self.znorm[self.g]>0)[0]]
        self.sursol_d=np.array(self.d)[np.where(self.znorm[self.d]>0)[0]]
        
    def point2D(self,ind):
        """Retourne le couple de coordonnées x,y du point ind
    
        :param ind: l'identifant du point
        :type ind: int
        :returns: couple de coordonnées x,y du point ind
        :rtype: array
        """   
        return np.array([self.x[ind],self.y[ind]]).reshape(2,1)
    
    # def point3D(self,ind):
    #     """Retourne le triplet de coordonnées x,y,z du point ind
    
    #     :param ind: l'identifant du point
    #     :type ind: int
    #     :returns: triplet de coordonnées x,y,z du point ind
    #     :rtype: array
    #     """
    #     return np.array([self.x[ind],self.y[ind],self.z[ind]]).reshape(3,1)
    
    # def point3D_origin(self,ind):
    #     """Retourne le triplet de coordonnées x,y,z du point d'origine ind
    
    #     :param ind: l'identifant du point
    #     :type ind: int
    #     :returns: triplet de coordonnées x,y,z du point d'origine ind
    #     :rtype: array
    #     """
    #     return np.array([self.x_origin[ind],self.y_origin[ind],self.z_origin[ind]]).reshape(3,1)
    
    def point_sensor(self,ind):
        """Retourne le triplet de coordonnées x,y,z du point d'origine ind et le temps gps
    
        :param ind: l'identifant du point
        :type ind: int
        :returns:  triplet de coordonnées x,y,z du point d'origine ind et le temps gps
        :rtype: array
        """
        return np.array([self.x_origin[ind],self.y_origin[ind],self.z_origin[ind]]).reshape(3,1),self.GPS_time[ind]

    def RANSAC(self,coords,Niter,seuil):
        """Ransac qui renvoie les points de la droite approximant le mieux les points
    
        :param coords: la liste des points à filtrer
        :type coords: list
        :param Niter: le nombre d'itérations avant la décision
        :type Niter: int
        :param seuil: le seuil de tolérance à la droite 
        :type seuil: float
        :returns: la liste des coordoonnées des points validés par la droite la plus plébiscitée
        :rtype: list
        """        

        nbpoints=len(coords)
        bestinlier=0
        for i in range(Niter):
            print('Iter= ',i)
            nbinlier=0
            p1,p2=np.random.choice(nbpoints,2,replace=False)
            p1_,p2_=coords[p1],coords[p2]
            pt1,pt2=self.point2D(p1_),self.point2D(p2_)
            n=normale(pt1,pt2)
            for j in range(nbpoints):
                ind=coords[j]
                dist=np.abs(np.dot(n.T,self.point2D(ind)-pt1))
                if dist<seuil:
                    nbinlier+=1
            if nbinlier>bestinlier:
                bestdroite=[pt1,pt2]
                bestinlier=nbinlier
        inliers,outliers=[],[]
        n=normale(bestdroite[0],bestdroite[1])
        for j in range(nbpoints):
            num_pt=coords[j]
            dist=np.abs(np.dot(n.T,self.point2D(num_pt)-pt1))
            if dist<seuil:
                inliers.append(num_pt)
            elif dist>seuil:
                outliers.append(num_pt)
        return inliers
    
    def dislater(self):
        """Fonction qui calcule les distances latérales grâce aux coordonnées capteur
        """
        self.dislat=((self.range**2-self.x_sensor**2)**0.5)
        
    def fichier_horaire(self):
        #fichier horaire
        v,d,t,p=[],[0],[],[]
        min_xs=np.min(self.x_origin_sensor)
        mins_xs=[i for i in range(nb_data) if self.x_origin_sensor[i]==min_xs]
        nb_line=len(mins_xs)
        p1,t_1=self.point_sensor(0)
        t.append(t_1)
        #projeté du premier point capté sur la droite des deux premiers points de la trajectoire
        line_approx_0=self.point_sensor(mins_xs[0])[0]-self.point_sensor(mins_xs[1])[0]
        line_approx_0=line_approx_0/np.sum(line_approx_0**2)**0.5#vecteur unitaire
        projete=self.point_sensor(mins_xs[0])[0]-p1
        p1=self.point_sensor(mins_xs[0])[0]-np.dot(projete.T,line_approx_0)*line_approx_0
        p.append(p1)
        for j in range(nb_line):
            p2,t_2=self.point_sensor(mins_xs[j])
            vec=p2-p1
            dist=np.sum(vec**2)**0.5
            vit=dist/(t_2-t_1)
            v.append(vit)
            d.append(dist)
            p1,t_1=p2,t_2
            t.append(t_2)
            p.append(p2)
        p2,t_2=self.point_sensor(nb_data-1)
        t.append(t_2)
        #projeté du dernier point capté sur la droite des deux derniers points de la trajectoire
        line_approx_1=self.point_sensor(mins_xs[-2])[0]-self.point_sensor(mins_xs[-1])[0]
        line_approx_1=line_approx_1/np.sum(line_approx_1**2)**0.5
        projete2=self.point_sensor(mins_xs[-1])[0]-p2
        p2=self.point_sensor(mins_xs[-1])[0]-np.dot(projete2.T,line_approx_1)*line_approx_1
        p.append(p2)
        vec=p2-p1
        vit=np.sum(vec**2)**0.5/(t_2-t_1)
        v.append(vit)
        d.append(dist)
        #self.p=p #les points de cette trajectoire
        self.v,self.d,self.t=v,d,t
    
    def time_to_get(self):
        #Je veux un point tous les
        #self.dis_horaire=0.1#mètres
        reste=0.0
        self.temps_a_recuperer=[self.t[0]]
        nb_v=len(self.v)
        for k in range(nb_v):
            compteur=0
            to_do=self.d[k+1]
            while to_do>self.dis_horaire:
                compteur+=1
                if reste:
                    self.temps_a_recuperer.append(self.temps_a_recuperer[-1]+(self.dis_horaire-reste)/self.v[k]+reste/self.v[k-1])
                    to_do-=(self.dis_horaire-reste)
                    reste=0
                else:
                    self.temps_a_recuperer.append(self.temps_a_recuperer[-1]+self.dis_horaire/self.v[k])
                    to_do-=self.dis_horaire
            reste+=to_do
            
    def point_to_keep(self):
        intx,inty,etendue_x,etendue_y,accum_g=accumulation(x[sg],y[sg],0.25)
        intxd,intyd,etendue_xd,etendue_yd,accum_d=accumulation(x[sd],y[sd],0.25)
        matseuilg=np.array([accum_g>self.nbaccu])[0]
        matseuild=np.array([accum_d>self.nbaccu])[0]
        
        self.temps_a_recuperer.append(GPS_time[-1])
        nbtemps=len(self.temps_a_recuperer)
        dislatgauche=np.zeros((nbtemps))
        dislatdroit=np.zeros((nbtemps))
        indice_temps=1
        indice_point=0
        besoin_droite,besoin_gauche = True, True 
        while indice_point<nb_data:
            if self.temps_a_recuperer[indice_temps]<GPS_time[indice_point]:
                indice_temps+=1
                besoin_gauche=True
                besoin_droite=True
            if indice_point in sg and besoin_gauche:#point du sursol gauche
                point=np.where(sg==indice_point)[0][0]
                if matseuilg[intx[point]][inty[point]]: #point de la façade par accumulation
                    besoin_gauche=False#assignation au temps: ok
                    dislatgauche[indice_temps-1]=dislat[indice_point]
            elif indice_point in sd and besoin_droite:
                point=np.where(sd==indice_point)[0][0]
                if matseuild[intxd[point]][intyd[point]]: #point de la façade par accumulation
                    besoin_droite=False#assignation au temps: ok
                    dislatdroit[indice_temps-1]=dislat[indice_point]
            if besoin_droite == False and besoin_gauche == False:
                indice_point=bisect(GPS_time,self.temps_a_recuperer[indice_temps+1])
                indice_temps+=1
                besoin_gauche=True
                besoin_droite=True
                continue
            indice_point+=1
            if not indice_point%100000:
                print(indice_point)
        self.dislatd=dislatdroit
        self.dislatg=dislatgauche
        
    
    def update_data(self):
        data_out=[]
        indice_temps=1
        for indice_point in range(len(self.x)):
            if self.temps_a_recuperer[indice_temps]<GPS_time[indice_point]:
                indice_temps+=1
            if indice_point in self.sursol_d:
                if np.abs(self.dislat[indice_point]-self.dislatd[indice_temps])<self.tolerance:
                    data_out.append(self.data_[indice_point])
            elif indice_point in self.sursol_g:
                if np.abs(self.dislat[indice_point]-self.dislatg[indice_temps])<self.tolerance:
                    data_out.append(self.data_[indice_point])  
            else:
                data_out.append(self.data_[indice_point])
            if not indice_point%1000:
                print(indice_point)
        self.data_out=data_out
        
    def nuage_out(self):
        vertex=np.array(self.data_out,self.dtype)
        plycloud = PlyElement.describe(vertex, self.name_,comments=self.coms)
        PlyData([plycloud]).write(self.outfile)

        
def stats(lis,nom):
    print("Max de "+nom, np.max(lis))
    print("Min de "+nom, np.min(lis))
    print("Moyenne de "+nom, np.mean(lis))
    print("STD de "+nom, np.std(lis))
    print("Mediane de "+nom,np.median(lis))
    
if __name__ == "__main__":
    test=Ouverture_ply("mairie12_520_2_LAMB93_000022.ply",0.1)
    test.ouvrir_nuage()
    nb_data =3000000
    GPS_time,x,y,z = test.GPS_time[0:nb_data],test.x[0:nb_data],test.y[0:nb_data],test.z[0:nb_data]
    rang,theta,x_origin,y_origin,z_origin = test.range[0:nb_data],test.theta[0:nb_data],test.x_origin[0:nb_data],test.y_origin[0:nb_data],test.z_origin[0:nb_data]

    
    
    x_sensor,y_sensor,z_sensor = test.x_sensor[0:nb_data],test.y_sensor[0:nb_data],test.z_sensor[0:nb_data]
    x_origin_sensor,y_origin_sensor,z_origin_sensor,phi = test.x_origin_sensor[0:nb_data],test.y_origin_sensor[0:nb_data],test.z_origin_sensor[0:nb_data],test.phi[0:nb_data]

 
    #theta_n,theta_p=theta_n/cpt_nega,theta_p/cpt_posi
    #print((-theta_n+theta_p-np.pi)/2)
    #n=100
    #ind = np.argpartition(theta, -n)[-n:]
    #topn = theta[ind]
    #stats(topn,'top100')
    #plt.figure()
    #plt.scatter(x,y,s=1)
    t1=time.time()
    test.d_g_grossier()
    d,g=test.d,test.g
    test.rang_grossier()
    t2=time.time()
    print(t2-t1)
    test.sursol_grossier()
    t3=time.time()
    print('toc',t3-t2)
    
    sg,sd=test.sursol_g,test.sursol_d
    stats(rang[g],'dis g')
    stats(rang[sg],'dis sur_g')
    stats(rang[d],'dis d')
    stats(rang[sd],'dis sur_d')
    mod_r=(rang[g]-np.mean(rang[g]))/np.std(rang[g])
    g=np.array(g)
    """
    g_filter=rang[g[np.where(mod_r<4)]]
    stats(g_filter,'bestg')
    mod_d=(rang[sd]-np.mean(rang[sd]))/np.std(rang[sd])
    d_filter=sd[np.where(mod_d<4)]
    stats(d_filter,'bestd')
    nb_iter=1
    test.f_d=test.RANSAC(test.sursol_d,nb_iter,0.2)
    test.f_g=test.RANSAC(test.sursol_g,nb_iter,0.2)
    t4=time.time()
    print('tac',t4-t3)
    test.f_d=np.array(test.f_d)
    test.f_g=np.array(test.f_g)
    
    xfd,yfd=test.x[test.f_d],test.y[test.f_d]
    plt.figure()
    plt.scatter(x[d],y[d],s=0.2)  
    plt.scatter(xfd,yfd)
    plt.title('droite + facade')
    plt.figure()
    plt.scatter(x[sd],y[sd],s=0.5)
    plt.title('sursol droit')
    #plt.scatter(x[g],y[g],s=0.5)  
    plt.figure()
    plt.scatter(xfd,yfd)
    plt.title('facade droite')
    xfg,yfg=test.x[test.f_g],test.y[test.f_g]
    plt.figure()
    plt.scatter(x[g],y[g],s=0.2)  
    plt.scatter(xfg,yfg)
    plt.title('gauche + facade')
    plt.figure()
    plt.scatter(x[sg],y[sg],s=0.5)
    plt.title('sursol gauche')
    #plt.scatter(x[g],y[g],s=0.5)  
    plt.figure()
    plt.scatter(xfg,yfg)
    plt.title('facade gauche')
    plt.figure()
    plt.scatter(x[d],y[d],s=0.2)  
    plt.scatter(xfd,yfd)
    plt.scatter(x[g],y[g],s=0.2)  
    plt.scatter(xfg,yfg)
    plt.title('facades')
    plt.figure()
    plt.scatter(x[d],y[d],s=0.2)  
    plt.scatter(x[g],y[g],s=0.2) 
    """
    test.dislater()


  
    dislat=test.dislat
    """
    stats(dislat[sg],'dsg')
    stats(dislat[d],'dsd')
    stats(dislat[d_filter],'bd')
    print(np.argmax(rang),d[np.argmax(dislat[d])])
    plt.hist(dislat[d],bins=200)
    plt.figure()
    plt.hist(dislat[sd],bins=200)
    plt.hist(dislat[test.f_d])
    plt.figure()
    plt.title("Nombre de points selon x_sensor")
    plt.hist(test.x_sensor,bins=50,range=(-50,10))
    plt.figure()
    plt.title("Nombre de points selon theta")
    plt.hist(test.theta,bins=100,range=(-np.pi,np.pi))
    print(stats(test.theta,"theta"))
    """
    test.fichier_horaire()
    test.time_to_get()
    test.point_to_keep()
    test.update_data()
    #test.nuage_out()
"""
    #fichier horaire
    
    v,d,t,p=[],[0],[],[]
    min_xs=np.min(test.x_origin_sensor)
    mins_xs=[i for i in range(nb_data) if test.x_origin_sensor[i]==min_xs]
    nb_line=len(mins_xs)
    p1,t_1=test.point_sensor(0)
    t.append(t_1)
    line_approx_0=test.point_sensor(mins_xs[0])[0]-test.point_sensor(mins_xs[1])[0]
    line_approx_0=line_approx_0/np.sum(line_approx_0**2)**0.5
    projete=test.point_sensor(mins_xs[0])[0]-p1
    p1=test.point_sensor(mins_xs[0])[0]-np.dot(projete.T,line_approx_0)*line_approx_0
    p.append(p1)
    for j in range(nb_line):
        p2,t_2=test.point_sensor(mins_xs[j])
        vec=p2-p1
        dist=np.sum(vec**2)**0.5
        vit=dist/(t_2-t_1)
        v.append(vit)
        d.append(dist)
        p1,t_1=p2,t_2
        t.append(t_2)
        p.append(p2)
    p2,t_2=test.point_sensor(nb_data-1)
    t.append(t_2)
    line_approx_1=test.point_sensor(mins_xs[-2])[0]-test.point_sensor(mins_xs[-1])[0]
    line_approx_1=line_approx_1/np.sum(line_approx_1**2)**0.5
    projete2=test.point_sensor(mins_xs[-1])[0]-p2
    p2=test.point_sensor(mins_xs[-1])[0]-np.dot(projete2.T,line_approx_1)*line_approx_1
    p.append(p2)
    vec=p2-p1
    dist=np.sum(vec**2)**0.5
    vit=dist/(t_2-t_1)
    v.append(vit)
    d.append(dist)
    parcouru=np.cumsum(d)
    plt.figure()
    plt.title("Distance parcourue au temps t")
    plt.plot(t,parcouru)
    plt.figure()
    plt.title("vitesse au temps t")
    plt.plot(t[1:],v)
    plt.show()
    
    #Je veux un point tous les
    dis_horaire=0.4#mètres
    reste=0.0
    temps_a_recuperer=[t[0]]
    nb_v=len(v)
    t_act=t[0]
    for k in range(nb_v):
        c=0
        v_act=v[k]
        to_do=d[k+1]
        while to_do>dis_horaire:
            c+=1
            if reste:
                temps_a_recuperer.append(temps_a_recuperer[-1]+(dis_horaire-reste)/v_act+reste/v[k-1])
                to_do-=(dis_horaire-reste)
                reste=0
            else:
                temps_a_recuperer.append(temps_a_recuperer[-1]+dis_horaire/v_act)
                to_do-=dis_horaire
            #rint(to_do)

        reste+=to_do
        

        
    intx,inty,etendue_x,etendue_y,accum_g=accumulation(x[sg],y[sg],0.25)
    intxd,intyd,etendue_xd,etendue_yd,accum_d=accumulation(x[sd],y[sd],0.25)
    nbaccu=1000
    matseuilg=np.array([accum_g>nbaccu])[0]
    matseuild=np.array([accum_d>nbaccu])[0]
    
    temps_a_recuperer.append(GPS_time[-1])
    nbtemps=len(temps_a_recuperer)
    dislatgauche=np.zeros((nbtemps))
    dislatdroit=np.zeros((nbtemps))
    indice_temps=1
    indice_point=0
    besoin_droite,besoin_gauche = True, True 
    while indice_point<nb_data:
        if temps_a_recuperer[indice_temps]<GPS_time[indice_point]:
            indice_temps+=1
            besoin_gauche=True
            besoin_droite=True
        if indice_point in sg and besoin_gauche:#point du sursol gauche
            point=np.where(sg==indice_point)[0][0]
            if matseuilg[intx[point]][inty[point]]: #point de la façade par accumulation
                besoin_gauche=False#assignation au temps: ok
                dislatgauche[indice_temps-1]=dislat[indice_point]
        elif indice_point in sd and besoin_droite:
            point=np.where(sd==indice_point)[0][0]
            if matseuild[intxd[point]][intyd[point]]: #point de la façade par accumulation
                besoin_droite=False#assignation au temps: ok
                dislatdroit[indice_temps-1]=dislat[indice_point]
        if besoin_droite == False and besoin_gauche == False:
            indice_point=bisect(GPS_time,temps_a_recuperer[indice_temps+1])
            indice_temps+=1
            besoin_gauche=True
            besoin_droite=True
            continue
        indice_point+=1
        if not indice_point%100000:
            print(indice_point)
"""